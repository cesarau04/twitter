// Fill out your copyright notice in the Description page of Project Settings.


#include "STwitterBorder.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STwitterBorder::Construct(const FArguments& InArgs)
{
	TAttribute<FString> Image = InArgs._Image;
	TAttribute<FString> Header = InArgs._Header;
	TAttribute<FString> Tweet = InArgs._Tweet;
	ChildSlot
	[
		SNew(SBorder)
		.Padding(5.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SNew(SBox)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Padding(1.0f)
				[
					SNew(STextBlock)
					.Text(FText::FromString(Image.Get()))
				]
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				[
					SNew(STextBlock)
					.Text(FText::FromString(Header.Get()))
				]
				+ SVerticalBox::Slot()
				[
					SNew(STextBlock)
					.Text(FText::FromString(Tweet.Get()))
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
