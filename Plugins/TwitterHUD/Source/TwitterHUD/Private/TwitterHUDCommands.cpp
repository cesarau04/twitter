// Copyright Epic Games, Inc. All Rights Reserved.

#include "TwitterHUDCommands.h"

#define LOCTEXT_NAMESPACE "FTwitterHUDModule"

void FTwitterHUDCommands::RegisterCommands()
{
	UI_COMMAND(OpenPluginWindow, "TwitterHUD", "Bring up TwitterHUD window", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
