// Fill out your copyright notice in the Description page of Project Settings.


#include "TweetData.h"

FString* UTweetData::GetUsername()
{
	return &Username;
}

FString* UTweetData::GetTweet()
{
	return &Tweet;
}

bool* UTweetData::GetShouldExists()
{
	return &bShouldExists;
}

void UTweetData::SetUsername(FString InUsername)
{
	Username = InUsername;
}

void UTweetData::SetTweet(FString InTweet)
{
	Tweet = InTweet;
}

void UTweetData::SetShouldExists(bool InBool)
{
	bShouldExists = InBool;
}
