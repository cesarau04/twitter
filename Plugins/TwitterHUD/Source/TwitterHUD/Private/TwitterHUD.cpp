// Copyright Epic Games, Inc. All Rights Reserved.

#include "TwitterHUD.h"
#include "TwitterHUDStyle.h"
#include "TwitterHUDCommands.h"
#include "LevelEditor.h"
#include "Widgets/Docking/SDockTab.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/SBoxPanel.h"
#include "ToolMenus.h"

static const FName TwitterHUDTabName("Twitter Tweets");

#define LOCTEXT_NAMESPACE "FTwitterHUDModule"

void FTwitterHUDModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module

	FTwitterHUDStyle::Initialize();
	FTwitterHUDStyle::ReloadTextures();

	FTwitterHUDCommands::Register();

	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FTwitterHUDCommands::Get().OpenPluginWindow,
		FExecuteAction::CreateRaw(this, &FTwitterHUDModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FTwitterHUDModule::RegisterMenus));

	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(TwitterHUDTabName, FOnSpawnTab::CreateRaw(this, &FTwitterHUDModule::OnSpawnPluginTab))
		.SetDisplayName(LOCTEXT("FTwitterHUDTabTitle", "TwitterHUD"))
		.SetMenuType(ETabSpawnerMenuType::Hidden);

}

void FTwitterHUDModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FTwitterHUDStyle::Shutdown();

	FTwitterHUDCommands::Unregister();

	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(TwitterHUDTabName);
}

TSharedRef<SDockTab> FTwitterHUDModule::OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs)
{
	return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.AutoHeight()
			[
				SNew(STextBlock)
				.Text(FText::AsCultureInvariant("THE BEST LIST EVER"))
			]
			+ SVerticalBox::Slot()
			[
				SAssignNew(ListWidget, SSomeListWidget)
			]
		];
}

void FTwitterHUDModule::PluginButtonClicked()
{
	FGlobalTabmanager::Get()->InvokeTab(TwitterHUDTabName);
}

void FTwitterHUDModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FTwitterHUDCommands::Get().OpenPluginWindow, PluginCommands);
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FTwitterHUDCommands::Get().OpenPluginWindow));
				Entry.SetCommandList(PluginCommands);
			}
		}
	}
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FTwitterHUDModule, TwitterHUD)


/*SNew(SScrollBox)
			.ScrollBarAlwaysVisible(true)
			+ SScrollBox::Slot()
			[
				SNew(STextBlock)
				.Text(FText::AsCultureInvariant("Cesar"))
			]
			+ SScrollBox::Slot()
			[
				SNew(STextBlock)
				.Text(FText::AsCultureInvariant("Augusto"))
			]
			+ SScrollBox::Slot()
			[
				SNew(STextBlock)
				.Text(FText::AsCultureInvariant("Garcia"))
			]
			+ SScrollBox::Slot()
			.HAlign(HAlign_Center)
			[
				SNew(STextBlock)
				.Text(FText::AsCultureInvariant("Perez"))
				.ToolTipText(FText::AsCultureInvariant("Last Tool Tip"))
			]*/