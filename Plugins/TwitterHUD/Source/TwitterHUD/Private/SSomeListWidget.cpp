// Fill out your copyright notice in the Description page of Project Settings.


#include "SSomeListWidget.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSomeListWidget::Construct(const FArguments& InArgs)
{
	SetAttribute(ChildSlot.SlotPadding, InArgs._Padding, EInvalidateWidgetReason::Layout);	
	ChildSlot
	[
		SNew(SListView<UTweetData*>)
		.ItemHeight(5)
		.ListItemsSource(&Items)
		.OnGenerateRow(this, &SSomeListWidget::OnGenerateRowForList)
	];

	UTweetData* AData (NewObject<UTweetData>());
	AData->SetUsername("cesarau04");
	AData->SetTweet("Fuimonos");
	Items.Add(AData);

	UTweetData* AData2 (NewObject<UTweetData>());
	AData2->SetUsername("marta90");
	AData2->SetTweet("hola mundo");
	Items.Add(AData2);
	//FText::FromName(GEngine->GameViewport->GetFName()).ToString()
}


TSharedRef<ITableRow> SSomeListWidget::OnGenerateRowForList(UTweetData* InItem, const TSharedRef<STableViewBase>& OwnerTable)
{
	return SNew(STableRow<UTweetData*>, OwnerTable)
		.Padding(2.0f)
		[
			SNew(STwitterBorder)
			.Image("A Image")
			.Header(*InItem->GetUsername())
			.Tweet(*InItem->GetTweet())
		];
}

END_SLATE_FUNCTION_BUILD_OPTIMIZATION