// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TweetData.generated.h"

/**
 * 
 */
UCLASS()
class TWITTERHUD_API UTweetData : public UObject
{
	GENERATED_BODY()

private:
	FString Username;
	FString Tweet;
	bool bShouldExists;

public:
	FString* GetUsername();
	FString* GetTweet();
	bool* GetShouldExists();

	void SetUsername(FString InUsername);
	void SetTweet(FString InTweet);
	void SetShouldExists(bool InBool);
};
