// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "TwitterHUDStyle.h"

class FTwitterHUDCommands : public TCommands<FTwitterHUDCommands>
{
public:

	FTwitterHUDCommands()
		: TCommands<FTwitterHUDCommands>(TEXT("TwitterHUD"), NSLOCTEXT("Contexts", "TwitterHUD", "TwitterHUD Plugin"), NAME_None, FTwitterHUDStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > OpenPluginWindow;
};