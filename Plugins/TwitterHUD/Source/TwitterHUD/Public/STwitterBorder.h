// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
class TWITTERHUD_API STwitterBorder : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STwitterBorder)
		: _Content()
		, _Image()
		, _Header()
		, _Tweet()
	{}
	SLATE_DEFAULT_SLOT(FArguments, Content)
	SLATE_ATTRIBUTE(FString, Image)
	SLATE_ATTRIBUTE(FString, Header)
	SLATE_ATTRIBUTE(FString, Tweet)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);
};
