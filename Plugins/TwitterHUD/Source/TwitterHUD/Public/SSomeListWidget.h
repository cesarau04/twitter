// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Widgets/Views/STableRow.h"

#include "./STwitterBorder.h"
#include "./TweetData.h"
/**
 * 
 */
class TWITTERHUD_API SSomeListWidget : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSomeListWidget)
		: _Content()
		, _Padding(FMargin(20.0f))
	{}
	SLATE_DEFAULT_SLOT(FArguments, Content)
	SLATE_ATTRIBUTE(FMargin, Padding)

	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	/* Adds a new TextBox with the string to the list */
	TSharedRef<ITableRow> OnGenerateRowForList(UTweetData* InItem, const TSharedRef<STableViewBase>& OwnerTable);

	/* The list of strings */
	TArray<UTweetData*> Items;

	/* The actual UI list */
	TSharedPtr<SListView<UTweetData*>> ListViewWidget;
};
