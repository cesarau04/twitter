#include "TwitterHTTP.h"

ATwitterHTTP::ATwitterHTTP()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATwitterHTTP::BeginPlay()
{
	Super::BeginPlay();
	Http = &FHttpModule::Get();
	MaxTweetBuffer = 5;
	CurrentBuffetPosition = 0;
	LastBytesReceived.Init(0, 5);
}

void ATwitterHTTP::CreateNewRequest()
{
	TSharedRef<IHttpRequest> ANewRequest = Http->CreateRequest();
	HttpRef = ANewRequest;
	ANewRequest = Http->CreateRequest();
	ANewRequest->SetVerb("GET");
	ANewRequest->SetURL("https://api.twitter.com/2/tweets/search/stream");
	ANewRequest->SetHeader("Authorization", "Bearer AAAAAAAAAAAAAAAAAAAAAFq%2BJgEAAAAAHLwX%2FaPTKrRarTOZ0LESjgHbw%2F8%3DeULVaOfG3ZtwXyNjY0XcccHScCC5h533ZzF9MuULDqQl54iBmj");
	ANewRequest->OnProcessRequestComplete().BindUObject(this, &ATwitterHTTP::OnComplete);
	ANewRequest->OnRequestProgress().BindUObject(this, &ATwitterHTTP::OnProgress);
	ANewRequest->ProcessRequest();
}

void ATwitterHTTP::OnProgress(FHttpRequestPtr HttpRequest, int32 BytesSent, int32 BytesReceived)
{
	if (MaxTweetBuffer == 0)
	{
		/*int32 LastBytes = LastBytesReceived[CurrentBuffetPosition];
		int32 remainingBytes = HttpRequest->GetContent().Num() - LastBytes;

		TArray<uint8> NewPayload;
		NewPayload.AddUninitialized(remainingBytes);
		FMemory::Memcpy(NewPayload.GetData(), 
												&(HttpRequest->GetContent()[LastBytes]), 
												remainingBytes * sizeof(uint8));
		HttpRequest->SetContent(NewPayload);*/
		FString Response = HttpRequest->GetResponse()->GetContentAsString();
		GEngine->AddOnScreenDebugMessage(5, 5.f, FColor::Green, Response);
		HttpRequest->GetResponse()->
		HttpRequest->SetContent(NewPayload);
	}
	else
	{
		MaxTweetBuffer = MaxTweetBuffer - 1;
		FString Response = HttpRequest->GetResponse()->GetContentAsString();
		int32 length = HttpRequest->GetResponse()->GetContent().Num();
		GEngine->AddOnScreenDebugMessage(5, 5.f, FColor::Green, Response);
		LastBytesReceived[CurrentBuffetPosition] = BytesReceived;
	}
	CurrentBuffetPosition = ((CurrentBuffetPosition + 1) % 5);
	// GEngine->AddOnScreenDebugMessage(2, 5.f, FColor::Red, FString::Printf(TEXT("Sent: %d"), BytesSent));
	// GEngine->AddOnScreenDebugMessage(3, 5.f, FColor::Red, FString::Printf(TEXT("Received: %d"), BytesReceived));
	// GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("Num: %d"), length));
}

void ATwitterHTTP::OnComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)
{
	// GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Complete Request");
}

void ATwitterHTTP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATwitterHTTP::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	if (HttpRef.IsValid())
	{
		HttpRef.Get()->CancelRequest();
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Cancel Request");
	}
}
