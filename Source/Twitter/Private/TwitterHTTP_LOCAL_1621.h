// C�sar Augusto Garc�a P�rez and XR Tales

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HttpModule.h"
#include "Templates/SharedPointer.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "Engine/Engine.h"
#include "GenericPlatform/GenericPlatformProcess.h"
#include "Containers/UnrealString.h"
#include "TwitterHTTP.generated.h"


UCLASS()
class ATwitterHTTP : public AActor
{
	GENERATED_BODY()
	
public:	
	ATwitterHTTP();

	UFUNCTION(BlueprintCallable, Category="HTTP")
	void CreateNewRequest();

protected:
	virtual void BeginPlay() override;

private:
	FHttpModule* Http;

public:
	void OnComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded);
	virtual void Tick(float DeltaTime) override;
};
