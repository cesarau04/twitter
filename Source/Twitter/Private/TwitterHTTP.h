// C�sar Augusto Garc�a P�rez and XR Tales

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HttpModule.h"
#include "Templates/SharedPointer.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "Engine/Engine.h"
#include "GenericPlatform/GenericPlatformProcess.h"
#include "Containers/UnrealString.h"
#include "Templates/SharedPointer.h"
#include "HAL/UnrealMemory.h"
#include "TwitterHTTP.generated.h"

UCLASS()
class ATwitterHTTP : public AActor
{
	GENERATED_BODY()
	
public:	
	ATwitterHTTP();

	UFUNCTION(BlueprintCallable, Category="HTTP")
	void CreateNewRequest();

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	FHttpModule* Http;
	TSharedPtr<IHttpRequest> HttpRef;
	
	// How many tweets to save in memory.
	int8 MaxTweetBuffer;
	
	// Helper to know how many bytes to remove in front of array.
	int8 CurrentBuffetPosition;

	// Array for saving n tweets bytes size.
	TArray<int32> LastBytesReceived;

public:
	void OnProgress(FHttpRequestPtr HttpRequest, int32 BytesSent, int32 BytesReceived);
	void OnComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded);
	virtual void Tick(float DeltaTime) override;
};
