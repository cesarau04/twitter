// Copyright Epic Games, Inc. All Rights Reserved.

#include "Twitter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Twitter, "Twitter" );
