// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TwitterGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TWITTER_API ATwitterGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
